const mysql = require('mysql2')

const openconnection=()=>{
    const connection = mysql.createConnection({

        port:3306,
        user:'root',
        password:'root',
        host:'demodb',
        database:'mydb'
    })

    connection.connect()

    return connection
}

module.exports={
    openconnection,
}