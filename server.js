const express = require('express')
const cors = require('cors')
const db = require('./db')
const utils = require('./utils')

const app = express()

app.use(cors('*'))

app.use(express.json())

app.get('/get',(request,response)=>{
    const connection=db.openconnection()

    const statement = `select book_id,book_title,publisher_name,author_name from Book`
    connection.query(statement,(error,records)=>{
        connection.end()

        response.send(utils.createResult(error,records))
    })
})

app.post('/add',(request,response)=>{
    const connection=db.openconnection()

    const {book_title,publisher_name,author_name}=request.body

    const statement = `insert into Book (book_title,publisher_name,author_name)
    values('${book_title}','${publisher_name}','${author_name}')`

    connection.query(statement,(error,result)=>{
        connection.end()

        response.send(utils.createResult(error,result))
    })
})


app.put('/update',(request,response)=>{
    const connection=db.openconnection()

    const {book_id,publisher_name,author_name}=request.body

    const statement = `update Book set publisher_name='${publisher_name}',
    author_name='${author_name}' where book_id='${book_id}'`

    connection.query(statement,(error,result)=>{
        connection.end()

        response.send(utils.createResult(error,result))
    })
})

app.delete('/delete/:book_id',(request,response)=>{
    const connection=db.openconnection()

    const {book_id}=request.params

    const statement = `delete from Book where book_id='${book_id}'`

    connection.query(statement,(error,result)=>{
        connection.end()

        response.send(utils.createResult(error,result))
    })
})


app.listen(4000,'0.0.0.0',()=>{
    console.log(`server started on port 4000`)
})
